#

import scipy.special as special
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
import os.path
import pickle
import math

# our funcs \/

from djangolib import *

import sgd_extractor_rand
import tp
import obj_to_file as o2f
import mobi_extractor_rand


def dualExtract_sgd_tp_rand(  n_randreplicates, datasets ,feature   ):

    # TP
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t\t'+'extracting tp data...'

    sgd_features_cai, sgd_features_weight, sgd_features_fop, sgd_features_gravy, sgd_features_length = tp.get_true_positive_result(datasets)
#       ^ lists of proteins's term values

    feature_list_tp = [ (float(a[0]), float(a[1])) for a in eval(feature) if (a[0]!=None and a[1] !=None)]
#       ^ depending on the argument given in the function, one of these terms is picked   ^ this "and" is v important!!
#                           ^ float() so that we dont get data-types that confuse downstream functions
    print '\t',len(eval(feature))

    feature_list_tp_T = zip(*feature_list_tp)

    o2f.obj_to_file(feature_list_tp_T,'feature_list_tp_T', dims=2)
    
    #### Calculates max sampleSize
    sampleSize = len(feature_list_tp_T[0])
#                               ^ the number of ppis available in our database, i.e the max sampleSize     #
    # RAND
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t\t'+'extracting rand data...'

    results_dict            = sgd_extractor_rand.get_n_trueNegative_results( n_randreplicates*sampleSize, sgd_features, [feature] )    # ??
#                                                                               ^ the number of "slices" of equal shape to tp
    feature_list_rand       = results_dict[feature]  # ??
    feature_list_rand       = [ (float(a[0]), float(a[1])) for a in feature_list_rand ]     # converts data into floats!
    print len(feature_list_rand)

    feature_list_rand_T     = zip(*feature_list_rand)

    # DEBUGGER FILE:
    o2f.obj_to_file(feature_list_rand_T, 'feature_list_rand_T', dims=2)

    # BOTH:
    return feature_list_tp_T, feature_list_rand_T




def dualExtract_mobi_tp_rand (  n_randreplicates, datasets, method  ):
    
    # TP
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t\t'+'extracting tp data...'
    ### \/ Anais Code ###
    rd                  = tp.get_tp_disorder(datasets,method)      #arg 1 is method of calculating disorder
    disorder_tp         = [ [interaction[1][0]/float(interaction[1][0]+interaction[0][0]),interaction[1][1]/float(interaction[0][1]+interaction[1][1])]  for interaction in zip(*rd)]
    
    print '\t',len(disorder_tp)
    disorder_tp         = [ (float(a[0]), float(a[1])) for a in disorder_tp if (a[0]!=None and a[1] !=None)]
    print '\t',len(disorder_tp)

    disorder_tp         = zip(*disorder_tp)
    disorderA_tp        = disorder_tp[0]
    disorderB_tp        = disorder_tp[1]

    num_tp_samples      = len(disorderA_tp)

    ### Translate to Andy code: ###
    sampleSize          = num_tp_samples
    feature_list_tp_T   = disorder_tp

    # RAND
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t\t'+'extracting rand data...'
                ### \/ Anais Code ###
    rd = mobi_extractor_rand.get_n_rand_results(n_randreplicates*sampleSize,mobi_disorder,[ 'mobi_disorder_Tally_Zero',\
                                                                                            'mobi_disorder_Tally_One',\
                                                                                            'mobi_disorder_method'],method)     
    zeros               = rd["mobi_disorder_Tally_Zero"]
    ones                = rd["mobi_disorder_Tally_One"]
    ones_zeros          = [zeros] + [ones]
    disorder_tn         = [ [interaction[1][0]/float(interaction[1][0]+interaction[0][0]),interaction[1][1]/float(interaction[0][1]+interaction[1][1])]  for interaction in zip(*ones_zeros)]

    print '\t',len(disorder_tn)
    disorder_tn         = [ (float(a[0]), float(a[1])) for a in disorder_tn if (a[0]!=None and a[1] !=None)] # @@ might not need
    print '\t',len(disorder_tn)

    disorder_tn         = zip(*disorder_tn)
    disorderA_tn        = disorder_tn[0]
    disorderB_tn        = disorder_tn[1]
    ### /\
    feature_list_rand_T =  disorder_tn      # big array needs to be chunkered to shapes equal to the tp_array

    # BOTH
    #--------------------------------------------------------------------------------------------------------------------------------
    return feature_list_tp_T, feature_list_rand_T




def datasets_2_filename(source, datasets, feature, n_randreplicates):

    # generate filename based on dataset and feature used: 
    filename_clean = source + '_' + ''.join(datasets).replace('/','') + '_' + feature + str(n_randreplicates)
#                                           ^ clean up buggy chars
    out_file_name = str('zmap_fig_impFix_'+filename_clean)    # auto-names file
    return out_file_name



def zmapper( data_tp, data_rand, resolution, n_randreplicates, filename, col_range = None, z_cutoff = 250):
#                                                                              ^ rand replications  
#           ^#tps   ^protein property  e.g. 'sgd_features_cai'
#                                 ^tp set e.g. ['CCSB-YI1']
#                                              ^ #rand reps to show in fig as scatter e.g. 5
#                                                            ^ list specifying dimensions of grid e.g. [100,100]
#                                                                                             ^ absolute colour range e.g. [-100,100]
    def chunker(iterable, chunksize):
        for i,c in enumerate(iterable[::chunksize]):
            yield iterable[i*chunksize:(i+1)*chunksize]

    # COMPARISON MAP
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'Specifying the comparison map...'

    # #population_arrays      = np.ndarray(([n_randreplicates,2,len(data_rand[0])])) # stores scatter points of each sample --> plot markers after
    # population_lists        = [] # stores scatter points of each sample --> plot markers after

    axis_range = list(data_rand[0]) + list(data_rand[1]) + list(data_rand[1]) + list(data_rand[0])

    xmin = min(axis_range)     # does x correspond to [0] or [1] ??
    xmax = max(axis_range)
    ymin = min(axis_range)
    ymax = max(axis_range)

    #print xmin,xmax,ymin,ymax
    xbins = str(resolution[0]) + 'j'
    ybins = str(resolution[1]) + 'j'

    X, Y        = np.mgrid[xmin:xmax:eval(xbins), ymin:ymax:eval(ybins)]
    positions   = np.vstack([X.ravel(), Y.ravel()])

    # KDEs 
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'KDE for tps...'

    kernel_tp           =   stats.gaussian_kde( data_tp )
    #kernel_tp           =   stats.gaussian_kde( data_tp, bw_method = 0.4 )
    #                                                 ^ bandwidth }} default = 0.2 - 0.28

    density_array_tp    =   np.reshape(kernel_tp(positions).T, X.shape) # define a matrix of coordinates used to make the comparisons later // @ coordinates generate
    

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'KDE for population...'

    density_arrays_rand     = np.ndarray(( [ n_randreplicates ] +  list(X.shape) ))          # n_randreplicates by (x by y) --> x by y = coarse map grid, and n_randreplicates of them

    sampleSize              = len(data_tp[0])

    data_rand_unzip         = np.array(data_rand).T.tolist()

    for i, sample in enumerate(chunker( data_rand_unzip , sampleSize)): # "cuts'n_randreplicates'folds" giant array into a deck of cards
        sample                      = zip(*sample)
        kernel                      = stats.gaussian_kde( sample )
        #kernel                      = stats.gaussian_kde( sample , bw_method = 0.4 )
        #                                                               ^ bandwidth }} default = 0.2 - 0.28


        #stats.gaussian_kde(sample)



        density_arrays_rand[i]      = np.reshape(kernel(positions).T, X.shape)
        print '\t\t'+str(i)+' done'

    # Z TESTS
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'applying z-tests'

#     ### REMOVE ZEROS        --> if one element hasd 
#     ######################################################################################################
#     density_arrays_both = np.concatenate(   (density_arrays_rand,[density_array_tp])   , axis=0)
#     #                         ^ joins ararys to allow easy iteration through them below   ^ joins by the 0th axis
#     #                                                                ^ have to [ ] this in order to the array shapes to match

#     for array in density_arrays_both:
#         for i,row in enumerate(array):
#             for j,col in enumerate(row):
#                 if array[i,j] == 0.:
# #                      \/ replaces the element at index of all 'n_randreplicates' arrays so that there are no clashes
#                     for m in range(len(density_arrays_both)):
#                         density_arrays_both[m,i,j] = float('NaN')
#     #####################################################################################################





    z_scores = stats.zmap(density_array_tp, density_arrays_rand, axis=0)
    #p_values = 1 - special.ndtr(z_scores)

    # #---------------------------------------------------
    # # Z-score distribution:
    # z_scores_flat   = z_scores.flatten()
    # z_scores_flat   = z_scores_flat.tolist()
    # z_scores_sorted = sorted(z_scores_flat)
    # # Log-scale z-score distribution
    # for i,z in enumerate(z_scores_sorted):
    #     z_scores_sorted[i] = math.log( math.fabs( z_scores_sorted[i]) )
    # x               = range(len(z_scores_sorted))
    # # Plot distribution:
    # plt.plot( x,z_scores_sorted )
    # # shows plot
    # plt.show()
    # # closes plot
    # plt.close()
    # #---------------------------------------------------



    # DEBUGGER FILE:
    o2f.obj_to_file(z_scores,'z_scores',dims=2)


    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'plotting & saving...'

    #z_cutoff = 250   # @@@@WARN: removes buggy color scaling

    if col_range == None: # if no absolute color range, assume relative colour range
        zmin = 0
        zmax = 0

        for i,row in enumerate(z_scores):
            for j,col in enumerate(row):                
                # exhaustative scan of 
                if col == -np.inf:
                    continue
                    #z_scores[i,j] = zmin
                else:
                    if col > -z_cutoff:       # prevent zmin being << than z_cutoff
                        if col < zmin:
                            zmin = col
                if col == +np.inf:
                    continue
                    #z_scores[i,j] = zmax
                else:
                    if col < +z_cutoff:       # prevetn zmax being >> z_cutoff
                        if col > zmax:
                            zmax = col
        # Replace the infs with max z-vals
        for i,row in enumerate(z_scores):
            for j,col in enumerate(row):
                if col == -np.inf:
                    z_scores[i,j] = zmin
                if col == +np.inf:
                    z_scores[i,j] = zmax
                if col < -z_cutoff:
                    z_scores[i,j] = zmin
                if col > +z_cutoff:
                    z_scores[i,j] = zmax

        col_range = [zmin,zmax]
        print '\t',zmin, zmax

    # shows kernel estimations:
    plt.imshow(np.rot90(z_scores), cmap= plt.cm.jet, extent= [xmin,xmax,ymin,ymax], vmin= col_range[0], vmax= col_range[1])

    # places a bar for the colors 
    plt.colorbar()

    # saves plot w/ filename generated by filename function
    plt.savefig(filename)                      # saves file

    # closes plot
    plt.close()

def zmapper_all():

    n_randreplicates    = 30
    resolution          = [ 200 , 200 ]
    col_range           = None

    datasets_list       = [ ['Combined-AP/MS'],\
                            ['s_cerevisiae_ss'],\
                            ['CCSB-YI1']]

    sgd_feature_list    = [ 'sgd_features_cai',\
                            'sgd_features_weight',\
                            'sgd_features_gravy',\
                            'sgd_features_fop',\
                            'sgd_features_length']

    mobi_methods_list   = [ u'disembl-465',\
                            u'disembl-HL',\
                            u'espritz-disprot',\
                            u'espritz-xray',\
                            u'espritz-nmr',\
                            u'iupred-long',\
                            u'iupred-short',\
                            u'seq-conserv']

    for datasets in datasets_list:

        
        # MOBI:
        ########
        for mobi_method in mobi_methods_list:

            print '\t'+'extracting mobi data:'
            filename            = datasets_2_filename('mobi', datasets, mobi_method, n_randreplicates)
            filename_pickle     = str(filename+'.txt')

            if os.path.exists( filename_pickle ) == False:
                # Extract the data:
                data_tp, data_rand  = dualExtract_mobi_tp_rand( n_randreplicates,  datasets,  mobi_method )
                # Pickle the data:
                out_file    = open( filename_pickle ,'w')
                obj         = [data_tp, data_rand]
                pickle.dump(obj, out_file)
                out_file.close()

            else:       # if a pickle does exist --> \/
                print '\t'+' Pickle exists --> super speed extraction enabled! '
                # Extract data from existing pickle:
                in_file     = open( filename_pickle )
                
                data_both   = pickle.load(in_file)
                
                data_tp     = data_both[0]
                data_rand   = data_both[1]

                in_file.close()

            # Generate KDEs and z-maps:
            zmapper( data_tp, data_rand, resolution, n_randreplicates, filename,  col_range = None, z_cutoff = 250 )    # zmaps with changing args "mobi-method"

        # SGD:
        ########
        for sgd_feature in sgd_feature_list:

            print '\t'+'extracting sgd data:'
            filename            = datasets_2_filename('sgd', datasets, sgd_feature, n_randreplicates)
            #                                          ^ source ^dataset used  ^feature mapped  ^number of reps used              
            filename_pickle     = str(filename+'.txt')

            if os.path.exists(filename_pickle) == False:   # if a pickle does not exist -->  \/
                # Extract data:
                data_tp, data_rand  = dualExtract_sgd_tp_rand( n_randreplicates,  datasets,  sgd_feature )

                # Pickle the data:
                out_file    = open( filename_pickle ,'w')
                obj         = [data_tp, data_rand]
                pickle.dump(obj, out_file)
                out_file.close()

            else:       # if a pickle does exist --> \/
                print '\t'+' Pickle exists --> super speed extraction enabled! '
                # Extract data from existing pickle:
                in_file     = open( filename_pickle )
                
                data_both   = pickle.load(in_file)
                
                data_tp     = data_both[0]
                data_rand   = data_both[1]

                in_file.close()

            # Generate KDEs and z-maps:
            zmapper( data_tp, data_rand, resolution, n_randreplicates, filename, col_range = None, z_cutoff = 250 )     # zmaps with changing args "sgd_feature"

    print '\t'+'Mission Accomplished!  ^___^'

if __name__ == '__main__':

    zmapper_all()

