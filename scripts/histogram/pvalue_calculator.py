#

import scipy.special as special
import numpy as np
import scipy.stats as stats
import sgd_extractor_rand
import tp
from djangolib import *
import matplotlib.pyplot as plt

# e.g. n = 10, samplesize = 4781, feature = ['sgd_features_cai'], datasets = ['CCSB-YI1']

def pvalue_calculator(n,            samplesize,     feature,        datasets,  reveal_rand,                     resolution):
#                      ^ rand replications  ^#tps    ^protein property  ^tp set  ^ #rand reps to show in fig       ^ list specifying dimensions of grid

    def chunker(iterable, chunksize):
        for i,c in enumerate(iterable[::chunksize]):
            yield iterable[i*chunksize:(i+1)*chunksize]

    #--------------------------------------------------------------------------------------------------------------------------------

    #\/ Data is extracted both in tp and tn BEFORE specifying the map --> mins and maxs can only be known here


    ####################        
    # DATA EXTRACTION  #
    ####################
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'extracting tp data from django...'

    sgd_features_cai, sgd_features_weight, sgd_features_fop, sgd_features_gravy, sgd_features_length = tp.get_true_positive_result(datasets)
#       ^ lists of proteins's term values
    feature_list_tp = [ (float(a[0]), float(a[1])) for a in eval(feature) if (a[0]!=None and a[1] !=None)]
#       ^ depending on the argument given in the function, one of these terms is picked   ^ this "and" is v important!!
#                           ^ float() so that we dont get data-types that confuse downstream functions
    feature_list_tp_T = zip(*feature_list_tp)

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'extracting data for rand...'

    results_dict            = sgd_extractor_rand.get_n_trueNegative_results(n*samplesize ,sgd_features, [feature])    # ??
    feature_list_rand       = results_dict[feature]  # ??
    feature_list_rand       = [ (float(a[0]), float(a[1])) for a in feature_list_rand ]     # converts data into floats!
#          ^ @@@
    feature_list_rand_T     = zip(*feature_list_rand)


    ###################
    # COMPARISON MAP  #
    ###################
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'Specifying the comparison map...'

    #population_arrays      = np.ndarray(([n,2,len(feature_list_tp_T[0])])) # stores scatter points of each sample --> plot markers after
    population_lists        = [] # stores scatter points of each sample --> plot markers after

    axis_range = list(feature_list_rand_T[0]) + list(feature_list_rand_T[1]) + list(feature_list_tp_T[1]) + list(feature_list_tp_T[0])
    
    xmin = min(axis_range)     # does x correspond to [0] or [1] ??
    xmax = max(axis_range)
    ymin = min(axis_range)
    ymax = max(axis_range)

    #print xmin,xmax,ymin,ymax
    xbins = str(resolution[0]) + 'j'
    ybins = str(resolution[1]) + 'j'

    X, Y = np.mgrid[xmin:xmax:eval(xbins), ymin:ymax:eval(ybins)]
    positions   = np.vstack([X.ravel(), Y.ravel()])

    #########
    # KDEs  #
    #########
    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'KDE for tps...'

    kernel_tp           =   stats.gaussian_kde(feature_list_tp_T)         # generate kernel density estimations // 
    density_array_tp    =   np.reshape(kernel_tp(positions).T, X.shape) # define a matrix of coordinates used to make the comparisons later // @ coordinates generate


    # DEBUGGER FILE:
    #####################################################
    out_file = open('debugger_tp.txt','w')              #
    for row in density_array_tp.tolist():               #
        line_tabbed = '\t'.join([str(i) for i in row])  #
        out_file.write(str(line_tabbed+'\n'))           #
    out_file.close()                                    #
    #####################################################

    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'KDE for population...'

    density_arrays_rand   = np.ndarray(([n] +  list(X.shape)))         # n by (x by y) --> x by y = coarse map grid, and n of them
    for i, sample in enumerate(chunker(feature_list_rand, samplesize)):  # "cuts'n'folds" giant array into a deck of cards
        sample                      = zip(*sample)
        population_lists.append(sample)
        kernel                      = stats.gaussian_kde(sample)
        density_arrays_rand[i]    = np.reshape(kernel(positions).T, X.shape)
        print '\t\t'+str(i)+' done'

    # DEBUGGER FILE:
    #########################################################
    out_file = open('debugger_rand.txt','w')                #
    for array in density_arrays_rand.tolist():              #
        for row in array:                                   #
            line_tabbed = '\t'.join([str(i) for i in row])  #
            out_file.write(str(line_tabbed)+'\n')           #
    out_file.close()                                        #
    #########################################################


    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'applying z-test --> calculating p values...'


    ### Removing zeros
    ##################################
    density_arrays_both = np.concatenate(   (density_arrays_rand,[density_array_tp])   , axis=0)
    #                         ^ joins ararys to allow easy iteration through them below   ^ joins by the 0th axis
    #                                                                ^ have to [ ] this in order to the array shapes to match
    for array in density_arrays_both:
        for i,row in enumerate(array):
            for j,col in enumerate(row):
                if array[i,j] == 0:
#                      \/ replaces the element at index of all 'm' arrays so that there are no clashes
                    for m, array in enumerate(density_arrays_both):
                        array[n,i,j] = float('NaN')
    ##################################

    z_scores = stats.zmap(density_array_tp, density_arrays_rand, axis=0)
    #p_values = 1 - special.ndtr(z_scores)


    #--------------------------------------------------------------------------------------------------------------------------------
    print '\t'+'plotting & saving...'

    fig=plt.figure()
    ax=fig.add_subplot(111)

    zmin = np.amin(z_scores)
    zmax = np.amax(z_scores)

    print zmin, zmax

    # shows kernel estimations:
    ax.imshow(np.rot90(z_scores), cmap= plt.cm.jet, extent= [xmin,xmax,ymin,ymax], vmin= -10., vmax= 10.)                                                                

    # scatter of average of population
    ######population_means = np.average(population_arrays, axis=0)

    for i in range(0,reveal_rand):
        #ax.plot(population_arrays[i][0], population_arrays[i][1],"g+",markersize=1.1)
        ax.plot(population_lists[i][0], population_lists[i][1],"g+",markersize=1)

    # draws scatter plot of interactor term values, a vs. b
    ax.plot(feature_list_tp_T[0], feature_list_tp_T[1],"m+",markersize=1)

    #--------------------------------------------------------------------------------------------------------------------------------
    ''' @@@@@@@@@@@ \/ really messy '''
    # saves plot to file
    for i,dataset in enumerate(datasets):
        if dataset == 'Combined-AP/MS':
            datasets[i] = 'Combined-APMS'

    out_file_name = str('zmap_'+ ''.join(datasets) + '_' + feature)    # auto-names file
    plt.savefig(out_file_name)                      # saves file

    print '\t'+'COMPLETE!'

if __name__ == '__main__':
    pvalue_calculator(10,1547,'sgd_features_cai',["CCSB-YI1"],3,[200,200])