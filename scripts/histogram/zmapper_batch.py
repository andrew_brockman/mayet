    
import scipy.special as special
import numpy as np
import scipy.stats as stats
from djangolib import *
import matplotlib.pyplot as plt

#functions by us:

import sgd_extractor_rand
import tp
import zmapper as zm
import obj_to_file as o2f

#-------------------
# initiations       | 
#-------------------

feature_names = None
dataset_names = None

#zmapper(10, 'sgd_features_cai', ["CCSB-YI1"], 5, [200,200], samplesize=None, crange = [-100,100])

def zmapper_batch(  n,  feature_names,  dataset_names,  reveal_rand,  resolution,  samplesize = None,  crange = None):
#                          ^ e.g. ['sgd_features_gravy','sgd_features_fop']
#                                        ^ e.g. ['CSB-YI1']
    for d in dataset_names:              # one list of term plots for each dataset
#       ^ for each dataset in the list ; need this as 'd' to prevent namespace clash

        print '___________________________________________________________'
        print 'Generating maps for '+str(d)+' dataset...'
        print '___________________________________________________________|'
        
        for f in feature_names:          # one plot for each feature
            print '\t'
            print '\t'+'__________________________________________________'
            print '\t'+'generating map for '+str(f)+':'
            print '\t'+'__________________________________________________|'

            zm.zmapper( n,  f,  d,  reveal_rand,  resolution, samplesize, crange)

feature_names = [   'sgd_features_weight',\
                    'sgd_features_cai',\
                    'sgd_features_fop',\
                    'sgd_features_length',\
                    'sgd_features_gravy'       ]

dataset_names = [   ['CCSB-YI1'],\
                    ['s_cerevisiae_ss'],\
                    ['Combined-AP/MS']         ]

zmapper_batch(  100,  feature_names,  dataset_names,  2, [400,400], samplesize=None, crange = None)