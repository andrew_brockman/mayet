from djangolib import *
import mobi_extractor_tn
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from djangolib import *



fig = plt.figure()
ax = fig.gca(projection = '3d')



rd = mobi_extractor_tn.get_n_trueNegative_results(4000,mobi_disorder,['mobi_disorder_Tally_Zero',\
							   'mobi_disorder_Tally_One',\
							   'mobi_disorder_method'],
							   'espritz-nmr')		

zeros  	= rd["mobi_disorder_Tally_Zero"]

ones 	= rd["mobi_disorder_Tally_One"]

ones_zeros = [ones] + [zeros]

disorder 	= [ [interaction[0][0]/float(interaction[1][0]+interaction[0][0]),interaction[0][1]/float(interaction[0][1]+interaction[1][1])]  for interaction in zip(*ones_zeros)]
disorder 	= zip(*disorder)
disorderA 	= disorder[0]
disorderB 	= disorder[1]

x, y = (disorderA, disorderB)	
hist, xedges, yedges	= np.histogram2d(x,y, bins = 50)

xpos, ypos = np.meshgrid(xedges[:-1]+0.25, yedges[:-1]+0.25)

xpos = xpos.flatten()/float(2)
ypos = ypos.flatten()/float(2)
zpos = np.zeros_like(xpos)
dx = xedges[1] - xedges[0]
dy = yedges[1] - yedges[0]
dz = hist.flatten()
ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color='#FFDBE2', zsort='average')
plt.xlabel('disorderA')			
plt.ylabel('disorderB')		
plt.title('disembl-465')

plt.savefig('disorder_tn.png')
plt.show()