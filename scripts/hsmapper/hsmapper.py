
from common.djangolib import *


import common.idmapper as idmapper
from yeastmapper.ymapper import write_ccsby_to_ppidb
import os
import sys

"""
This module is responsible for populating the hc_interactions table 
with human hc interactions.
"""

def read_ccsbhs( datapath_hsmapper , source, dataset_name, species_id ):
    """Description:

    This function populates the hc_interaction table with the 
    interactions described in the file at the filepath given by 
    datapath_ymapper. It acts on files formatted as per the 
    HI2_2011 dataset.

    Arguments:

    datapath_hsmapper:  This is the path to the yeast file containing 
                        the high confidence interactions. This should 
                        be a tab delimited file containing the 
                        systematic names of the proteins involved 
                        in the interactions.(Type=String)

    hc_source:          This is the source (in a literary sense) of 
                        the high confidence interactions by convention
                        this is a pubmed id. This will be inserted 
                        into the hc_interaction table. (Type=String)


    hc_dataset_name:    This is the name of the dataset as it is 
                        referred to at the source and elsewhere in 
                        the literature. This will be inserted into the
                        hc_interaction table.(Type=String)

    taxon_id:           This is the taxonomical id of the species the 
                        file pointed to by "datapath_ymapper".
                        (Type=String)

    Returns 

    None

    """
    try:
        ccsbyfn = os.path.normpath(sys.argv[1])
    except IndexError:

        # Uses data_dir to get around box-specific paths
        # datapath_ymapper = os.path.join( data_dir, "ymapper/" )
        # datapath_ymapper = os.path.join( datapath_ymapper, filename )

        ccsbyfn = datapath_hsmapper

    print "Reading input file: %s" % ccsbyfn

    bg_id_dict = idmapper.bgid_to_dbid_dict( "ENTREZ_GENE", bg_id_2_db_id = False )
    
    print "Opening: %s " % ccsbyfn

    ccsby_file = open(ccsbyfn, "r")

    for line in ccsby_file:
            #print line
            

            ccsby_line=line.split("\t")

            try:
                
                protid_a_from_line      = ccsby_line[0].strip()
                protid_a_from_line      = bg_id_dict[protid_a_from_line]

                protid_b_from_line      = ccsby_line[2].strip()
                protid_b_from_line      = bg_id_dict[protid_b_from_line]
                

            except KeyError:
                print "Dictionary lookup failed on line: %s" % line
                
                continue


            
            write_ccsby_to_ppidb(protid_a_from_line, protid_b_from_line, source, dataset_name, species_id)

    return






if __name__ == '__main__':
    read_ccsbhs("../../data/hsmapper/HI2_2011.tsv", "PMID: 21516116", "HI2_2011", "9606")


