#!/usr/bin/Rscript

###################
# crossvalidation #
###################


# crossvalidation.R is a script to calculate the permuted importance of features in an rscript it is based on a heuristic method.
# Example usage:
#
# Rfpimp.R -r [response file] -p [predictor file] -o [output file]
#
# The response file is the list of responses or classes one per row
#
# The predictor file is a csv file containing the features of each of the elements. The features should be represented by columns.
# The first row should contain the feature names.
#
# The output file is a saved r object containing the accuracies of the rrandom forest generated at each iteration of the crossvalidation




library(caret)
library(getopt)
library(party)
library(randomForest)

spec = matrix(c(
  "response_file"  , "r", 1, "character",
  "predictor_file" , "p", 1, "character",
  "output_path"    , "o", 1, "character"
), byrow = TRUE, ncol=4)


opt = getopt(spec)


if ( is.null(opt$response_file  ) )    { opt$response_file     = "lhead"  }
if ( is.null(opt$predictor_file ) )    { opt$predictor_file    = "fhead"  }
if ( is.null(opt$output_path    ) )    { opt$output_path       = "accuracies"}



numfolds = 10

predictors = read.csv(opt$predictor_file, header= TRUE)
response   = as.factor(read.csv(opt$response_file, colClasses=("numeric"), header=FALSE)[,1])

folds = createFolds(t(response), k = 10, list = TRUE, returnTrain = FALSE)

predictors$Response = response

accuracies = data.frame(matrix(ncol=2, nrow=numfolds))
colnames(accuracies) = c("T_Acc", "F_Acc")

for(i in seq(1,numfolds)){
  testing  = predictors[folds[[i]],]
  print(dim(testing))
  training = predictors[which(!(seq(1,length(response)) %in% folds[[i]]), arr.ind=TRUE),]
  print(dim(training))
  rf.f  =  randomForest(Response ~ ., data=training)
  confmatrix=table(testing$Response, predict(rf.f,
                                             newdata = testing))
  acc.pc=c(confmatrix[1,1]/sum(confmatrix[1,]), confmatrix[2,2]/sum(confmatrix[2,]))
  print(acc.pc)
  accuracies[i,]=acc.pc
  
}

save(accuracies, file=opt$output_path)

print("Means :")
print(colMeans(accuracies))
print("SDevs :")
print(apply(accuracies,2,sd))