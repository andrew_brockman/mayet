#!usr/bin/python 

import re
import sys
import time

import sys, os
#print os.path.dirname(sys.executable)

from common.djangolib import *
from django.core.exceptions import ObjectDoesNotExist

import common.idmapper as idmapper

def mobi_to_django( datapath_mobi, datapath_idmapper, startingLine = 1 ):
#                               ^ assigned by user in the config.cfg file --> specific to each box --> local directory with required data
#                               ^ this arg needs to be set in the meta_parser
#                       ^             ^@1                    ^@2
    
    """
    Description

    This function extracts disorder data from the mobi data file 
    "annotations_ATCC_204508_S288c.fasta" and parses it into the 
    ppidb database.

    Arguments

    datapath_mobi       Datapath for the mobi disroder data file
                        e.g. "annotations_ATCC_204508_S288c.fasta".
                        (Type=string)

    datapath_idmapper   Datapath for the idmapper module.
                        (Type=String)

    startingLine        The line of the datafile which the function
                        begins parsing from. This defaults to 1.
                        (Type=Integer)

    """

    # generates i_map dict
    print '\t\tgenerating mobi2biogrid map...'

    # Maps biogrid ids to mobi ids:
    in_filename = os.path.basename(datapath_mobi) # we run mobi parser twice: man, yeast --> one pickle for each! 
    in_filename = in_filename.partition('.')[0]
    # ^ specifies if we work with yeast or man @3

    mappings    = idmapper.bgid_to_dbid_dict( 'SWISSPROT', datapath_idmapper, bg_id_2_db_id = False )
#                                                       ^@3                                                        ^@2          ^ @1
#                                                              ^ assigned by user in the config.cfg file --> specific to each box --> local directory with required data

    # Initate progress bar:         # <-- if pickle not exist
    #---------------------------------
    t0              = time.clock()            # starts the timer -> can check total elapsed time
    lineCount       = 0
    lineCount_skip  = 0
    lineCount_total = 0
    #---------------------------------


    

    print '\tDetermining the number of methods...'


    # Initate progress bar:     # <-- if pickle exist
    #---------------------------------
    t0              = time.clock()            # starts the timer -> can check total elapsed time
    lineCount       = 0
    lineCount_skip  = 0
    lineCount_total = 0
    #---------------------------------

    # Initiate Method Collector
    #-----------------------------
    methods = []

    values = ''
    valueslist = []
    tally1 = 0
    tally0 = 0
    tallyDash = 0
    #-----------------------------

    # Open mobi data file:
    in_file = open( datapath_mobi  )
    #                               ^ @2

    # LOOP1: METHOD COLLECTION
    while True:

        in_line = in_file.readline()

        if in_line == "":
            break

        if in_line.startswith('>') and values == '':        # IF encountered new protein header -->        @        @2
            
            a = re.search('\>(.+)\|annotation', in_line )    # IF encountered new protein header -->   dsnt /\ cover it?
            if a != None:                                   # .. => if the match !=None == does have a match                

                splitline = in_line.split('|')              # split the header line... really?? @@@ WARN

                MobiID = splitline[0][1:]                   # store the protein name, ignoring the '>'
                MobiID2 = splitline[2]                      # store the fragment name }} whole seq will have frag name == protein name @1
                method = splitline[3]                       # store the method

                continue

        if in_line.startswith('>') and (values != ''):      # IF encountered new protein header -->
            if (MobiID == MobiID2):                         #.. and IF this protein is a WHOLE sequence @1
                
                methods.append(method)                      # collect the list of methods on the fly
                methods = sorted(set(methods))              # remove duplicates to form a set of unique methods
                
                valueslist.append([MobiID,values,method])

                a = re.search('\>(.+)\|annotation', in_line )

            # what is this doing here? @2 WARN \/ & ^
            if a != None:

                lineCount_total += 1                                             #  @3

                splitline = in_line.split('|')  
                MobiID = splitline[0][1:]
                MobiID2 = splitline[2]
                method = splitline[3]
                values = ''

        if in_line.startswith('>') != True:
            values += in_line.strip()



    lineCount_total = float(lineCount_total)

    print lineCount_total


# MOBI ANNOTATE:
#----------------------------------------------------------------------------------------------------------------

    print '\tAnnotating Proteins with disorder values...'
    for entry in valueslist:

        # LINE SKIPPING::
        #------------------------------------------------------------------------------------------------------------------------------------------------
        lineCount_skip += 1                     # number of lines counted   
        if (lineCount_skip - 1) < startingLine: # Skips until reaching line specified by "startingLine" arg @1
        #         ^    @ WARN: '-1' --> since the +1 to count does not start until this block is run => we must reference to the past
            continue

        #------------------------------------------------------------------------------------------------------------------------------
        # LOADING BAR::
        elapsedTime     = int(time.clock() - t0)                        # t is wall seconds elapsed (floating point)
        lineCount       += 1
        percentageLeft  = int(  ((lineCount_total - (lineCount+startingLine))*100 )/lineCount_total  )     # percentage of job left
        rate            = int( lineCount / (elapsedTime + 0.1) ) + 0.1                    # total lines per time elapsed
        #                                                      ^ prevent /0 err    
        eta             = int( (lineCount_total - (lineCount + startingLine) ) / rate)
        # Prints every 5 secs: 
        if elapsedTime%5 < 1.:  # print ever 5 seconds to prevent slow-down
            print("Remaining %",percentageLeft,"||   lines done:",lineCount+startingLine,"||   Seconds elapsed:",elapsedTime,"||   Rate:",rate,"||   ETA:",eta) #progress bar: percentage left to complete
        #------------------------------------------------------------------------------------------------------------------------------s

        # Unpack the entry
        #---------------------
        tally1 = 0
        tally0 = 0
        tallyDash = 0
        MobiID = entry[0]
        count = entry[1]
        method = entry[2]
        #---------------------
        
        a = re.search('[A-Z]',count)
        
        if (a != None):
            continue

        for number in count:
            if '1' in number:
                tally1 = tally1 + 1

            if '0' in number:
                tally0 = tally0 + 1

            if '-' == number:
                # if MobiID == MobiID2:      # commented out since MobID2 == MobiID1 
                continue            

        entry.append([tally1,tally0])

        try:
            biogrid_id_fromMappings     = mappings[MobiID]
#                                           ^ checks if the mapper has this mobiID, IF NOT: --> biogrid does not have this sgd_id           
            try:
                mobi_disorder_newEntry = mobi_disorder.objects.get_or_create    (   mobi_disorder_id           = MobiID,\
                                                                                    mobi_disorder_Tally_Zero   = tally0,\
                                                                                    mobi_disorder_Tally_One    = tally1,\
                                                                                    mobi_disorder_method       = method,\
                                                                                    interactor_key             = interactor.objects.get(biogrid_id = biogrid_id_fromMappings)  )[0]
#                                                                               ^ links new sgd-annotated-proteins to the biogrid proteins  ^adds new interactor_ids from sgd to our biogrid db, via the interactor key, IF NOT EXIST: --> sgd protein is added to db but cannot be linked to already existing biogrid proteins
            except ObjectDoesNotExist:
                #print(biogrid_id_fromMappings+'')       
                continue
        except KeyError:
            #print'Error- no match in BioGrid', MobiID    #there exists no match between the Mobi Id and the biogrid interactor Ids  
            continue
    try:   # If the pickle existed, there's no need to close the in_file..? @ WARN
        in_file.close()
    except NameError:
        pass

#if __name__ == '__main__':
#    mobi_to_django( os.path.abspath( sys.argv[1] ), os.path.abspath( sys.argv[2] ), startingLine = 0 )