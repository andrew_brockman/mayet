from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.http import HttpResponse, HttpResponseBadRequest
from models import * 
from django import *
import protein_form
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Avg, Max, Min, Count
from uploadedfilehandler import handle_uploaded_file
from django.core.servers.basehttp import FileWrapper
import csv
import cStringIO as StringIO
import random
from django.views.generic.simple import direct_to_template
from itertools import izip_longest
from randomforest.featuresandclasses import predict_pairs_rf

def statistics(request):
    empty = 0
    return render_to_response('base_stats.html',{'empty':empty})

def about(request):
    empty = 0
    return render_to_response('base_about.html',{'empty':empty})
    #print 'working'
    #return HttpResponse('HELLO')

def help(request):
    empty = 0
    return render_to_response('base_help.html',{'empty':empty})


def contact(request):
        
        """ 

        Description

        This function will send any request for 'contact' to the
        corresponding contact template

        Arguments

        request A request from the user on the site to see the contact
        page

        Returns

        The html template corresponding to the contact page and an
        empty dictionary for if in the future information might need
        to be stored
        """

        empty = 0
        return render_to_response('base_contact.html',{'empty':empty})

def download_table(request):


    """

    Description

    This function takes a user request to download the table from the
    site and stores the request as a session.

    Arguments

    request A request from the user on the site to download the table

    Returns

    The HttpResponse corresponding to a tab delimited text file named
    results_table.tsv

    """

    download_id=request.GET["download_id"]  


    proteins_list = request.session[download_id]


    my_table = StringIO.StringIO()                      
            
    csvwriter = csv.writer(my_table, delimiter='\t',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)

    csvwriter.writerow(["Protein_A", "Protein_B", "T_int_Y2H", "T_int_AP/MS"])

    a = zip(*proteins_list)

    protein_a       =   a[2]
    protein_b       =   a[3]

    t_int_y2h            =   a[6]
    t_int_apms           =   a[7]

    results_list    =   zip(protein_a, protein_b, t_int_y2h, t_int_apms)
    csvwriter.writerows(results_list)
    my_table.flush()

    response = HttpResponse(my_table.getvalue(), content_type='application/text')
    response['Content-Disposition'] = 'attachment; filename=results_table.tsv'
    return response



def get_synonyms(prota, protb,taxid,db_name):



    """
    Description

    This function captures the appropriate Biogrid ID corresponding to
    the users protein entry


    Arguments

    prota       The users first input protein 
    protb       The users second input protein
    taxid       The corresponding taxonomic ID to the interaction 
                species
    db_name  The name of the type of identification of the protein
                that the user has input

    Returns

    A string of Biogrid IDs corresponding to prota and protb respectively 
    """

    try:
        synonym_a   =   interactor_synonyms.objects.filter(synonym_db = db_name, synonym = prota).filter(interactor_key__species_key__taxon_id = taxid)[0].interactor_key
        synonym_b   =   interactor_synonyms.objects.filter(synonym_db = db_name, synonym = protb).filter(interactor_key__species_key__taxon_id = taxid)[0].interactor_key


    except IndexError:

        return None

    return synonym_a, synonym_b




def get_exact_match(model_class, filter_field, ids):

    """     
    Description

    This function searches for an exact match of a Biogrid interaction
    in the database

    Arguments

    model_class   class from models.py scheme to search through
    filter_field  Defines which filter to assign to the model class
    ids           Biogrid IDs

    Returns

    The Biogrid interaction match if the match exists

    """

    query = model_class.objects.annotate(count=Count(filter_field)).filter(count=len(set(ids)))
    
    for _id in ids:
        query = query.filter(**{filter_field:_id})

    return query.values()





def get_rf_prediction(interactor_pairs, taxon_id):

    """

    Description

    This function collects the data associated with the random forest predictions table in the models.py scheme

    Arguments

    biogrida Biogrid ID corresponding to the first input protein
    biogridb Biogrid ID corresponding to the second input protein

    Returns

    random forest data in a dictionary 

    """

    if taxon_id=="4932":

        kwargs = {
        "go_slim_name"          :       "goslim_yeast",
        "disorder_names_"       :       ['disembl-HL',"espritz-nmr"],
        "filter_for_sgd"        :       True,
        "filter_for_disorder"   :       True
        }


        y2h_conf     = predict_pairs_rf(interactor_pairs,   "y2h_gsd_sc.rf",  **kwargs).tolist()
        capms_conf   = predict_pairs_rf(interactor_pairs, "capms_gsd_sc.rf",  **kwargs).tolist()

    if taxon_id == "9606":

        kwargs = {

        "go_slim_name"          :       "goslim_generic",
        "disorder_names_"       :       ['disembl-HL',"espritz-nmr"],
        "filter_for_sgd"        :       False,
        "filter_for_disorder"   :       True

        }

        y2h_conf     = predict_pairs_rf(interactor_pairs, "y2h_gd_hs.rf",  **kwargs).tolist()
        capms_conf   = ["NA", "NA"]*len(y2h_conf)

    #print y2h_conf, capms_conf



    #print y2h_conf
    #print capms_conf

    #print type(y2h_conf)
    #print type(capms_conf)

    if type(y2h_conf) == tuple:
        y2h_conf    =   [list(y2h_conf)]
        capms_conf  =   [list(capms_conf)]

    else:
        y2h_conf    =   [[a[0],a[1]] for a in y2h_conf]
        capms_conf  =   [[a[0],a[1]] for a in capms_conf]

    #print y2h_conf
    #print capms_conf


    true_y2h_conf   = zip(*y2h_conf)[1]
    true_capms_conf = zip(*capms_conf)[1]

    true_conf       = zip(true_y2h_conf, true_capms_conf) 

    return  true_conf










"""    

    Description    

    The function takes a post submitted by the
    data in protein_form.py and checks that it is valid. This function
    returns a dictionary of data that corresponds to the information about
    the random forest, SGD data, disorder and any information that
    mightneed to be rendered on the html page. If the form is not valid,
    the function returns the user to the homepage so the user may input
    into the form again.

    Arguments

    request  A user request triggered by submitting a the
    protein_form.py

    returns

    A dictionary of data rendered to an html template page. OR, the
    original submission page.

"""

def protrequest(request):
    
    proteins_list   =   []
    disordera       =   []
    disorderb       =   []
    sgd_imputed_a   =   []
    sgd_a           =   []
    sgd_imputed_b   =   []
    sgd_b           =   []

    interactions    =   []

    if request.method == "POST":  
        #print request.POST                     #If the form has been submitted...
    
        form  = protein_form.Proteinsubmission(request.POST)        #A form bound to the POST data
        
        if form.is_valid():                             #All validation rules pass , process the data in form.cleaned_data
            
            data        = form.cleaned_data
            species     = (data['species'])
            db_name  = (data['identifier'])
            format   = (data["format"])

            x = ['Error', 'Error', 'Error', 'Error', 'Error','Error']    #need to figure out how many columns there will be and update this 

            #print request.FILES, "asdufhasldkjfhalksdfhkla"

            if  request.FILES == {}:
                proteins_   = (data['proteins']).split(';')

                for prot_interaction in proteins_:
                    prot_interaction = prot_interaction.split(',')
                

                    if len(prot_interaction) != 2:
                        x = ['','',prot_interaction,'**', species,'Protein Interaction must be of length two','-','-',db_name]
                        proteins_list.append(x)
                        continue
     

                    if len(prot_interaction) == 2: 
                        protein1    = prot_interaction[0].strip()
                        protein2    = prot_interaction[1].strip()

                    interactions.append([protein1, protein2])

            else:
                interactions = handle_uploaded_file(request.FILES['files'], format) 


            interactor_pairs    =   []
            synonym_list        =   []

            #print interactions

            for pair in interactions:
                if len(pair)!=2:
                    return HttpResponseBadRequest("<h1>400 Error</h1><p>Error parsing file, not all interactors composed of two proteins please check format.</p>")

            for pair in interactions:

                protein1 = pair[0]
                protein2 = pair[1]


                synonyms                =   get_synonyms(protein1,protein2,species,db_name)

                print synonyms

                if synonyms == None:
                    x       = ['','',protein1,protein2,species, 'We are unable to retrieve interaction data for these protein IDs ','-','-',db_name] 
                    proteins_list.append(x)


                if synonyms != None:
                    prot_a_bio      =   synonyms[0] 
                    prot_b_bio      =   synonyms[1]


                    synonym_list.append([protein1,protein2])
                    interactor_pairs.append([prot_a_bio, prot_b_bio])
            
            #print interactor_pairs
            #print synonym_list
            


            if interactor_pairs != []:
                true_scores = get_rf_prediction(interactor_pairs, species)
                
                #print true_scores
                #print interactor_pairs
                #print synonym_list

                for interactor_pair, synonyms,scores in izip_longest(interactor_pairs, synonym_list, true_scores):

                    y2h_tscore      =   scores[0]
                    capms_tscore    =   scores[1]

                    prot_a_bio      =   interactor_pair[0].biogrid_id
                    prot_b_bio      =   interactor_pair[1].biogrid_id

                    protein1        =   synonyms[0]
                    protein2        =   synonyms[1]

                    x   =   [prot_a_bio,prot_b_bio,protein1,protein2,species,'Interaction Detected. Click for Interaction Data', y2h_tscore, capms_tscore , db_name]
                    
                    proteins_list.append(x)


            download_id     =   '%030x' % random.randrange(256**15)

            request.session[download_id] = proteins_list

            return render_to_response('base_results.html', {'results': proteins_list, "dl_link":download_id})

    else: 
        form = protein_form.Proteinsubmission()                     #An unbound form 
                
    return render_to_response("base_inputs.html", {
        'form' : form
        })

    


def data(request,prota,protb,taxid,db_name):


    """

    Description 

    This function collects the data for rendering on the
    results page if the user requests the data.

    Arguments

    request     The request from the user to retreive the data from the
                database  
    prota       The first submitted protein by the user 
    protb       The second submitted protein by the user 
    taxid       The corresponding taxonomic ID to the input
                proteins 
    db_name     The name of the type of identification of
                the protein that the user has input

    Returns

    The function returns a dictionary of data corresponding to the
    available data in the database per specific interaction examined.
    OR, an HttpResponse that an Error has occured.

    """

    if request.method == "GET":

        proteins_list   =   []
        disordera       =   []
        disorderb       =   []
        sgd_imputed_a   =   []
        sgd_a           =   []
        sgd_imputed_b   =   []
        sgd_b           =   []
        go_slim_A       =   []
        go_slim_B       =   []



        synonyms        =   get_synonyms(prota,protb,taxid,db_name)    # synonyms shouold never equal none, if the user makes it this far to acquiring the data 
        
        if synonyms != None: 
            prot_a_bio      =   synonyms[0].biogrid_id
            prot_b_bio      =   synonyms[1].biogrid_id
            data_get_a      =   synonyms[0]
            data_get_b      =   synonyms[1]
            exact_match     =   get_exact_match(interaction, "interactor_m2m_key__biogrid_id", synonyms)
            mobi_data_a     =   data_get_a.mobi_disorder_set.filter(mobi_disorder_method__in=['espritz-nmr','disembl-HL']).values_list('mobi_disorder_method','imputed','mobi_disorder_Tally_One','mobi_disorder_Tally_Zero')
            mobi_data_b     =   data_get_b.mobi_disorder_set.filter(mobi_disorder_method__in=['espritz-nmr','disembl-HL']).values_list('mobi_disorder_method','imputed','mobi_disorder_Tally_One','mobi_disorder_Tally_Zero')
            go_slim_a       =   data_get_a.go_slim_set.values('go_slim_id')
            go_slim_b       =   data_get_a.go_slim_set.values('go_slim_id')
            sgd_list_a      =   data_get_a.sgd_features_set.values('sgd_features_fop','sgd_features_length','sgd_features_weight','sgd_features_cai','sgd_features_gravy')
            sgd_list_b      =   data_get_b.sgd_features_set.values('sgd_features_fop','sgd_features_length','sgd_features_weight','sgd_features_cai','sgd_features_gravy')
            


            for go in go_slim_a:
                go_slim_a = str(go['go_slim_id'])
                go_slim_A.append(go_slim_a)
            for go in go_slim_b:
                go_slim_b = str(go['go_slim_id'])
                go_slim_B.append(go_slim_b)

            if len(exact_match) == 0:


                for tally in mobi_data_a:
                    dis_prop_a      = (tally[2])/(tally[2]+tally[3])
                    dis_method_a    = str(tally[0])
                    imputed         = tally[1]
                    disordera.append([dis_method_a,dis_prop_a,imputed])
                    
                    

                for tally in mobi_data_b:
                    dis_prop_b      = (tally[2])/(tally[2]+tally[3])
                    dis_method_b    = str(tally[0])
                    imputed         = tally[1]
                disorderb.append([dis_method_b,dis_prop_b,imputed])

                
                x   = [prota,protb, "-", "-","-", disordera, go_slim_a, sgd_list_a, 
                        disorderb, go_slim_b, sgd_list_b]
                proteins_list.append(x)
                
            if len(exact_match) != 0 :


                for tally in mobi_data_a:
                    dis_prop_a      = (tally[2])/(tally[2]+ tally[3])
                    dis_method_a    = str(tally[0])
                    imputed         = tally[1]
                    disordera.append([dis_method_a,dis_prop_a,imputed])

                for tally in mobi_data_b:
                    dis_prop_b = (tally[2])/(tally[2]+tally[3])
                    dis_method_b    = str(tally[0])
                    imputed         = tally[1]
                    disorderb.append([dis_method_b,dis_prop_b,imputed])
    
            
                    
                x   = [prota,protb, "-", "-", "-", disordera,go_slim_A, sgd_list_a, 
                        disorderb, go_slim_B, sgd_list_b]
                proteins_list.append(x)
                
                
            return render_to_response('base_data.html',{'results':proteins_list})

        else: 
            return HttpResponse('ERROR')





