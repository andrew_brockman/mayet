
from django.db import models, connection
from django.db.models.sql import InsertQuery

###############################
#                             #
# model extensions            #
#                             #
###############################



class BatchInsertQuery( InsertQuery ):

    """
    Class that extends the InsertQuery class of  to allow the batch 
    inserts to be used. This should allow an increase in the speed 
    of mass writes to the database.
    """

    def as_sql(self):
        """
        Constructs a SQL statement for inserting all of the model 
        instances into the database.

        Differences from base class method:        

        - The VALUES clause is constructed differently to account 
        for the grouping of the values (actually, placeholders) into
        parenthetically-enclosed groups. I.e., VALUES (a,b,c),(d,e,f)
        """
        qn = self.connection.ops.quote_name
        opts = self.model._meta
        result = ['INSERT INTO %s' % qn(opts.db_table)]
        result.append('(%s)' % ', '.join([qn(c) for c in self.columns]))
        result.append( 'VALUES %s' % ', '.join( '(%s)' % ', '.join( 
            values_group ) for values_group in self.values ) ) # This line is different
        params = self.params
        if self.return_id and self.connection.features.can_return_id_from_insert:
            col = "%s.%s" % (qn(opts.db_table), qn(opts.pk.column))
            r_fmt, r_params = self.connection.ops.return_insert_id()
            result.append(r_fmt % col)
            params = params + r_params
        return ' '.join(result), params

    

    def insert_values( self, insert_values ):
        """
        Adds the insert values to the instance. Can be called 
        multiple times
        for multiple instances of the same model class.

        Differences from base class method:

        -Clears self.columns so that self.columns won't be duplicated for each
        set of inserted_values.        
        -appends the insert_values to self.values instead of extends so that
        the values (actually the placeholders) remain grouped separately for
        the VALUES clause of the SQL statement. I.e., VALUES (a,b,c),(d,e,f)
        -Removes inapplicable code
        """
        self.columns = [] # This line is new

        placeholders, values = [], []
        for field, val in insert_values:
            placeholders.append('%s')

            self.columns.append(field.column)
            values.append(val)

        self.params += tuple(values)
        self.values.append( placeholders ) # This line is different



class ManagerEx( models.Manager ):
    """
    Extended model manager class.
    """
    def batch_insert( self, *instances ):
        """
        Issues a batch INSERT using the specified model instances.
        """
        cls = instances[0].__class__
        query = BatchInsertQuery( cls, connection )
        for instance in instances:

             values = [ (f, f.get_db_prep_save( f.pre_save( instance, True ) ) ) \
                 for f in cls._meta.local_fields ]
             query.insert_values( values )

        return query.execute_sql()


###############################
#                             #
# models                      #
#                             #
###############################
class species(models.Model):
        """
        - Description:  species that interactors belong to


        - Relations:    

        Linked to by "species_key" foreign key field in hc_interactions and interactor tables


        - Fields:       

        taxon_id = The NCBI taxon Id of the interactors
        """ 
        #CharFields::
        taxon_id = models.CharField(max_length=150,unique=True)         # e.g. "taxid:9606" } in particular "9606" (?) // Header: "Taxid Interactor A:"&"Taxid Interactor B:"

                          
 


class interactor(models.Model):                                        
        """
        - Description:  proteins that have been detected to interact in biogrid, there will be two of these per interaction and belong to particular species
        - Relations:    

        many to many (2-to-1) relation with interaction; 
        species_key     =   Foreign key pointing at the species table 
        
        Linked to by "interactor_key" foreign key field in the interaction, mobi_disorder, sgd_features and interactor_synonyms tables

        - Fields:       

        biogrid_id      =   The NCBI biogrid id of each interactor
        
        """
        #CharFields::
        biogrid_id = models.CharField(max_length=150,unique=True)       # "entrez gene/locuslink:6416|BIOGRID:112315" // Header: "Interaction Identifiers"
	 
        #Foreign keys::
        species_key = models.ForeignKey(species)                                             
	

        objects                 =      ManagerEx()
        #ManyToMany::
        

class interaction(models.Model):                                        # proteins that have been
        """
        - Description:  interactions detected as part of some dataset published by some author(s) at some date using some method with some confidence score
        - Relations:    

        many-to-many relationship with interactor table
        many to many relationship with hc_interactions table

        - Fields:

        interaction_src_db_id   =       Id and database name of the interaction in its source database.  This is of the form "database name: id" e.g "BIOGRID:1233"
        interaction_type_id     =       Interaction type code (PSI-MI) and short description of the code
        method                  =       The method with which the interaction was observed
        method_format           =       The format for the ID of the method with which the interaction was observed
        publication_id          =       Id of the publication in which the interaction was observed
        publication_source      =       Source for the id of the publication
        publication_author      =       Primary author of the publication
        publication_year        =       Year in which the publication was published
        interactor_m2m_key      =       A m2m field relating the interactors to the interaction

        """
        

        interaction_src_db_id = models.CharField(max_length=150,unique=True) 
                                              
        interaction_type_id = models.CharField(max_length=150) 

        method = models.CharField(max_length=150)       
        method_format = models.CharField(max_length=150)     

        publication_id = models.CharField(max_length=150)   
        publication_source = models.CharField(max_length=150)          
        publication_author = models.CharField(max_length=150)          
 
        publication_year = models.IntegerField(max_length=150)         

        interactor_m2m_key  = models.ManyToManyField(interactor)



class rf_prediction(models.Model):
        """
        - Description:  A table containing the confidence scores in each interaction as calculated by the randomforest models

        - Relations:    

        interactor_a_key        =       A foreign key pointing at the interactor table has the related name "rf_prediction_set_b" (for reverse lookups)  
        interactor_b_key        =       A foreign key pointing at the interactor table has the related name "rf_prediction_set_a" (for reverse lookups) 

        - Fields:

        p_true                  =       The probability that the interaction is a true positive given that it was observed with the method given by the method field
        p_false                 =       The probability that the interaction is a false positive, false negative, or a true negative given that it was or was not observed with the 
                                        name method as given by the method field
        
        rf_name                 =       The name of the random forest used to calculate this score
        method                  =       The name of the experimental method for which the p_true and p_false are given
        mod_time                =       The modification time of the record

        This Model has a "unique together" constraint placed upon "method", "rf_name", "interactor_a_key", "interactor_b_key" whilst this won't prevent a duplicate record if interactor
        a and b are swapped this should provide some measure of protection against duplication.

        This Model also contains the object "ManagerEx" that allow for the batch insertion of values this increases the speed with which the data can be written into
        the database. As this table is very large this is a priority.

        """

        p_true                  =      models.DecimalField(decimal_places=4, max_digits=5)
        p_false                 =      models.DecimalField(decimal_places=4, max_digits=5)
        interactor_b_key        =      models.ForeignKey(interactor, related_name="rf_prediction_set_b")
        interactor_a_key        =      models.ForeignKey(interactor, related_name="rf_prediction_set_a")
        rf_name                 =      models.CharField(max_length=150)
        method                  =      models.CharField(max_length=150)
        mod_time                =      models.DateField(auto_now=True, auto_now_add=True)

        ##Objects

        objects                 =      ManagerEx()

        class Meta:
            unique_together     =      ("method", "rf_name", "interactor_a_key", "interactor_b_key")



class go_slim_type(models.Model):
        """
        - Description:  A table containing the names of the go slims used

        - Relations:    

        many to many relationship with go_slim table


        - Fields:

        go_slim_reference       =       The name of the goslim (as used by the go ontology website 
        here http://www.geneontology.org/GO.slims.shtml#avail) used to generate the goslim terms that this points to
        
        """
        go_slim_reference         = models.CharField(max_length = 150, unique = True)


class go(models.Model):

        """
        - Description:  A table containing the go ids pertinent to the proteins in our database
        

        - Relations:    

        many to many relationship with interactor table
        many to many relationship with go table


        - Fields:

        go_id                   =       The go id in question "GO:0042791"
        """

        go_id                    = models.CharField(max_length=150, unique = True)      

        interactor_m2m_key       = models.ManyToManyField(interactor)
        

class go_slim(models.Model):
        """
        - Description:  A table containing the go-slim ids pertinent to the proteins in our database
        
        - Relations:    

        many to many relationship with go_slim_type table
        many to many relationship with go table
        many to many relation with interactor table

        - Fields:

        go_slim_id              =      The go slim id in question "GO:0042791"

        """
        go_slim_id               = models.CharField(max_length = 150, unique = True)


        interactor_m2m_key           = models.ManyToManyField(interactor)
        go_slim_type_m2m_key         = models.ManyToManyField(go_slim_type)
        go_id_m2m_key                = models.ManyToManyField(go)




class hc_interactions(models.Model):

        """
        - Description:  A table which identifies the interactions in our database 
        - Relations:    

        interaction_key =              Foreign key pointing at the interaction table
        species_key     =              Foreign key pointing at the species table


        - Fields:

        source          =   The source of the hc_interactions this usually points to a paper
        dataset_name    =   The name of the dataset, normally the name it is referred to as in the paper

        """
        source          = models.CharField(max_length = 150)
        dataset_name    = models.CharField(max_length = 150)

        interaction_key = models.ForeignKey(interaction)
        species_key     = models.ForeignKey(species)


#----------------------------------------------------------------------------------------------
# 3. sgd_features: weight, cai, fop (frequency of opimal codons), gravy (hydrophobicity) #
#----------------------------------------------------------------------------------------------

class sgd_features(models.Model):

        """
        - Description:  A table which identifies which interactions in our database 

        - Relations:

        interactor_key                  =       A foreign key pointing at the interactor table

        - Fields:

        sgd_features_weight             =       The weight of the protein referred at using the foreign key
        sgd_features_cai                =       The Codon Adaptation Index (CAI) of the protein referred the foreign key
        sgd_features_fop                =       The frequency of optimal peptides (FOP)  of the protein referred the foreign key
        sgd_features_gravy              =       The grand average of hydropathy (GRAVY) score  of the protein referred the foreign key
        sgd_features_length             =       The length of the protein referred the foreign key

        sgd_features_weight_imputed     =       A boolean indicating whether or not the weight of the protein was imputed
        sgd_features_cai_imputed        =       A boolean indicating whether or not the CAI of the protein was imputed
        sgd_features_fop_imputed        =       A boolean indicating whether or not the FOP of the protein was imputed
        sgd_features_gravy_imputed      =       A boolean indicating whether or not the GRAVY of the protein was imputed
        sgd_features_length_imputed     =       A boolean indicating whether or not the length of the protein was imputed

        """

        sgd_features_weight         = models.FloatField(max_length = 150,null=True,blank=True)
        sgd_features_weight_imputed = models.BooleanField(default=False)

        sgd_features_cai            = models.FloatField(max_length = 150,null=True,blank=True)
        sgd_features_cai_imputed    = models.BooleanField(default=False)

        sgd_features_fop            = models.FloatField(max_length = 150,null=True,blank=True)
        sgd_features_fop_imputed    = models.BooleanField(default=False)


        sgd_features_gravy          = models.FloatField(max_length = 150,null=True,blank=True)
        sgd_features_gravy_imputed  = models.BooleanField(default=False)

        #integerfield:
        sgd_features_length         = models.IntegerField(max_length=150,null=True,blank=True)
        sgd_features_length_imputed = models.BooleanField(default=False)

        #Foreign key:
        interactor_key              = models.ForeignKey(interactor,unique=True) 

#---------------------------------------------------------------------------------------------------
# 3. Mobi_disorder 
#---------------------------------------------------------------------------------------------------

class mobi_disorder(models.Model):              #(Mobi ID--- one to one relationship with Biogrid ID because Mobi ID is linked to uniprot) 
        
        """
        - Description:  A table which identifies the interactions in our database 

        - Relations:
            
        interactor_key                  =       A foreign key pointing at the interactor table

        - Fields:

        mobi_disorder_id                =       The ID of the protein as stored in the MOBI disorder file. Mainly placed here for debug purposes
        mobi_disorder_method            =       The method with which the disorder was calculated.
        mobi_disorder_Tally_Zero        =       The tally of zeros along the protein. A zero indicates a disordered region. Field is a float to allow odd values for imputed proteins
        mobi_disorder_Tally_One         =       The tally of ones along the protein. A one indicates a disordered region. Field is a float to allow odd values for imputed proteins
        imputed                         =       A boolean indicating whether or not the disorder of the protein was imputed

        """

        mobi_disorder_id                         = models.CharField(max_length = 150) 
        mobi_disorder_method                     = models.CharField(max_length = 150)
        mobi_disorder_Tally_Zero                 = models.FloatField() 
        mobi_disorder_Tally_One                  = models.FloatField()
        imputed                                  = models.BooleanField(default=False)


        #Foreign Key: 
        interactor_key                           = models.ForeignKey(interactor)
        

class interactor_synonyms(models.Model):              #(Mobi ID--- one to one relationship with Biogrid ID because Mobi ID is linked to uniprot) 
        """
        - Description:  A table which maps contains synonyms for the interactors in our database

        - Relations:
            
        interactor_key      =       A foreign key pointing at the interactor table

        - Fields:

        synonym             =       A synonym used for the interactor pointed at using the interactor_key
        synonym_db          =       The database dor which the synonym is valid
        """



        #charfield:
        synonym             = models.CharField(max_length = 150)
        synonym_db          = models.CharField(max_length = 150)

        #foreignfield
        
        interactor_key      = models.ForeignKey(interactor)


        objects                 =      ManagerEx()

