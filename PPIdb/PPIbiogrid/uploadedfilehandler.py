"""

This module is designed to handle uploaded files containing protein 
ids that are handled by


"""

def handle_uploaded_file(int_file, format):
        
        """

        Description

        This function takes an uploaded file of a certain format and
        returns the interactions. 

        
        Arguments

        int_file                The file to be parsed (Type=filetype)

        format                  Format of the file (Type=filetype)

        Returns

        interactions    A list of lists in which the proteins in each
                                        interaction are placed.

        """

        def handle_tab2(int_file):
                """

                Description

                Helper function to handle .tab2 formatted files.

                """
                interactions = []

                for line in int_file:
                        if line.startswith("#"):
                                continue

                        else:
                                interactions.append(line.split("\t")[1:3])

                return interactions

        def handle_2col(int_file):

                """

                Description

                Helper function to handle files with a list of two tab 
                separated protein ids

                """

                interactions = []

                for line in int_file:
                        if line.startswith("#"):
                                continue

                        else:
                                splitline       =       line.split("\t")
                                
                                prota           =       splitline[0].strip()
                                protb           =       splitline[1].strip()

                                #print prota, protb

                                interactions.append([prota, protb])

                return interactions

        def handle_osp(int_file):

                """

                Description

                Helper function to handle files with a list of OSPREY formatted
                protein ids

                """

                interactions = []

                for line in int_file:
                        if line.startswith("GeneA"):
                                continue

                        else:
                                interactions.append(line.split("\t")[0:2])

                return interactions

        ### function starts here ####


        if format=="tab2":
                interactions=handle_tab2(int_file)

        elif format=="2col":
                interactions=handle_2col(int_file)

        elif format=="osprey":
                interactions=handle_osp(int_file)

        else:
                print "Error unrecognised input format"
                raise

        return interactions