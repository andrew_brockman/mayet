from django.conf.urls.defaults import *
from django.conf import settings 
from django.views.generic.simple import direct_to_template

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()


urlpatterns = patterns('',
    (r'^protrequest', 'PPIbiogrid.views.protrequest'),
    (r'^home', 'PPIbiogrid.views.protrequest'),
    (r'^$', 'PPIbiogrid.views.protrequest'),
    (r'^about','PPIbiogrid.views.about'),
    (r'^download_table','PPIbiogrid.views.download_table'),
    (r'^data/(.+)_(.+)_(\d+)_(\D+)','PPIbiogrid.views.data'),
    (r'^contact/', 'PPIbiogrid.views.contact'),
    (r'^help/','PPIbiogrid.views.help'),
    (r'^statistics/','PPIbiogrid.views.statistics')
)


if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )




